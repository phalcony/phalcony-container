Phalcony Developer Container Environment
========================================

What is included?
-----------------

* Ubuntu 14.04
* Nginx
* PHP-FPM 7.0

Who can use it?
-----------------

Anyone who needs to develop using Phalcony

How to use it?
-----------------

        git clone https://gitlab.com/phalcony/container
        cd container/docker
        sudo docker build ./ --tag phalcony_dev:1.0 

Buildtime takes aprox. 2m30s.

Now, to run the container the first time, you need to inform the local path for the Nginx folder
(/var/www/html), thhe APP_HOST_NAME, and APP_HOST_PORT you will use. map the same port inside
to the outside. You will be able to access your server using the URL:

http://{APP_HOST_NAME}:{APP_HOST_PORT}

        sudo docker run \
            --name projectname\
            -v ~/Projects/projectname:/var/www/html \
            -e APP_HOST_NAME='dockertest' \
            -e APP_HOST_PORT='8000' \
            -p 8000:8000 \
            -e USERID=`id -u $USER` \
            -e USERNAME=$USER \
            -d phalcony_dev:1.0

REMEMBER: You need to add the hostnames to your /etc/hosts file on your operating system


Disclaimer
-----------------

INTENDED FOR DEVELOPER ONLY! DO NOT USE IN PRODUCTION!

DON'T RUN THIS AS ROOT! RUN WITH SUDO! IT USES THE CURRENT LOGGED USER FOR FIX HOST DISK PERMISSIONS 

SERVER PORT - you need to choose a free unused port for each hostname


DEVELOPER FAST CLEAN AND BUILD

        sudo docker stop sample-phalcony && \
        sudo docker rm sample-phalcony && \
        sudo docker rmi phalcony_dev:0.1 && \
        sudo docker build ./ --tag phalcony_dev:0.1

        sudo docker run \
                --name sample-phalcony \
                -v ~/Projects/sample-phalcony:/var/www/html \
                -e APP_HOST_NAME='dockertest' \
                -e APP_HOST_PORT='8000' \
                -p 8000:8000 \
                -e USERID=`id -u $USER` \
                -e USERNAME=$USER \
                -d phalcony_dev:0.1


